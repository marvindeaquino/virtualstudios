<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'bY/kFiElQ56+nRX4KfajoHVSDvkvs3P16CrjomfWDjIvgsTIBpxJmWFsTI4KbsS4nEs+9k6buuEKgNbkkXzpSg==');
define('SECURE_AUTH_KEY',  'ZabC7DlgH/nF8Txwavo8HwLAKZ5IOFdE93t9RBs+sF2MRS3GHer8J7sJk9DVuRxWpYYrXXiAiO8C5zJ3Z41D8A==');
define('LOGGED_IN_KEY',    'RBrGrMOfv+MWZlL1K+l8BR5pU+UbEoxgsGgC5XH5+Q+xxYCTqaOdpR2/G2/uD8POG+XPw5/J31HN0tmZmTuHfg==');
define('NONCE_KEY',        '3HE5YXbKVbqEihUZUv+q1qhI28jx/kITzOGkmfSVXoq3FYG/POR0kq9bJhOyGx38zeOkuLd3B83TlFGrHCsfVA==');
define('AUTH_SALT',        'Sc0JdsxHcuK0CHIw4Z2iTzCzMbgRdNLFBFLqCoMl33fwICp0N0nunpG/KlHvXJI3jA5CbrUxjGx6OKCl+zhyQg==');
define('SECURE_AUTH_SALT', 'rD/esuX9jQXbGZZLPon4OHPWPwLbrW0AwoghxJUVtlxUNruabx0H79jWW80+GFv0GqHUpreBUwOFiyHl8naDtg==');
define('LOGGED_IN_SALT',   '9EZa6lcsg38QA+96GYzBiHBhQW72/8W6mJ4raBWFT0f49JUFqNZZ7Xu3mAIHqCwtjrccIVjISnx9Byv+mj5uqg==');
define('NONCE_SALT',       'r5+jZUfLHnIvSUa3UHjQfBXv5FcxT1h2igkklayHShDXRS+27kjXZPcvAb+dtfEX1R2hJXCSUVsd6aYmcGOSZQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';





/* Inserted by Local by Flywheel. See: http://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy */
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
	$_SERVER['HTTPS'] = 'on';
}
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
