<?php while (have_posts()) : the_post(); ?>


    <?php /*get_template_part('templates/content', 'page'); */?>

    <div class="body-wrap">
        <?php get_template_part('templates/page', 'header'); ?>

        <div class="main-section">
            <?php get_template_part('templates/flex', ''); ?>
        </div>
    </div>


  <style>
    <?php the_field('custom_css'); ?>
  </style>
<?php endwhile;
