/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

    // Use this variable to set up the common and page specific functions. If you
    // rename this variable, you will also need to rename the namespace below.
    var Sage = {
        // All pages
        'common': {
            init: function () {
                // JavaScript to be fired on all pages


              $('.navbar-toggler,.mobile-content-shadow').click(function() {
                $( 'body' ).toggleClass( "is-mobile-nav" );
              });


              $(document).ready(function () {
                    $('.owl-carousel').owlCarousel({

                        loop: true,
                        margin: 0,
                        nav: false,
                        items: 1
                    });
                });

                (function ($) {
                    $(".navbar-main-menu>li").each(function () {
                        sub_child = $(this).find('.dropdown-menu').children().length;
                        if (sub_child > 0) {
                            $(this).find('> a').append('<span class="expand"><i class="icon-chevron-arrow-down"></i></span>');
                        }
                    });

                    $('.navbar-main-menu>li>a>.expand').click(function (e) {
                        e.preventDefault();
                        $('.navbar-main-menu>li>a').find('.dropdown-menu').not($(this).parent('a').parent('li').find('.dropdown-menu')).slideUp(100);
                        $(this).parent('a').next('.dropdown-menu').slideToggle(100);
                    });
                    $(".mobile-nav .navbar-inline>li").each(function () {
                        sub_child = $(this).find('.sub-menu').children().length;
                        if (sub_child > 0) {
                            $(this).find('> a').append('<span class="expand"><i class="icon-chevron-arrow-down"></i></span>');
                        }
                    });

                    $('.mobile-nav .navbar-inline>li>a>.expand').click(function (e) {
                        e.preventDefault();
                        $('.mobile-nav .navbar-inline>li>a').find('.sub-menu').not($(this).parent('a').parent('li').find('.sub-menu')).slideUp(100);
                        $(this).parent('a').next('.sub-menu').slideToggle(100);
                    });


                })(jQuery);
                (function ($) {
                    var _createClass = function () {function defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}return function (Constructor, protoProps, staticProps) {if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;};}();function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}var FilterGallery = function () {
                        function FilterGallery() {_classCallCheck(this, FilterGallery);
                            this.$filtermenuList = $(".filtermenu li");
                            this.$container = $(".section-listing .row");
                            this.$containerhero = $(".section-listing-hero .row");

                            this.updateMenu("all");
                            this.$filtermenuList.on("click", $.proxy(this.onClickFilterMenu, this));
                        }_createClass(FilterGallery, [{ key: "onClickFilterMenu", value: function onClickFilterMenu(

                            event) {
                            var $target = $(event.target);
                            var targetFilter = $target.data("filter");

                            this.updateMenu(targetFilter);
                            this.updateGallery(targetFilter);
                        } }, { key: "updateMenu", value: function updateMenu(

                            targetFilter) {
                            this.$filtermenuList.removeClass("active");
                            this.$filtermenuList.each(function (index, element) {
                                var targetData = $(element).data("filter");

                                if (targetData === targetFilter) {
                                    $(element).addClass("active");
                                }
                            });
                        } }, { key: "updateGallery", value: function updateGallery(

                            targetFilter) {var _this = this;
                            if (targetFilter === "all") {
                                this.$container.fadeOut(300, function () {
                                    $(".post").show();
                                    _this.$container.fadeIn();
                                });
                                this.$containerhero.fadeOut(300, function () {
                                    $(".posthero.all").show();
                                    _this.$containerhero.fadeIn();
                                });
                                this.$containerhero.fadeOut(300, function () {
                                    $(".posthero-sub").hide();
                                    _this.$containerhero.fadeIn();
                                });
                            } else {
                                this.$container.find(".post").each(function (index, element) {
                                    _this.$container.fadeOut(300, function () {
                                        if ($(element).hasClass(targetFilter)) {
                                            $(element).show();
                                        } else {
                                            $(element).hide();
                                        }
                                        _this.$container.fadeIn();
                                    });
                                });
                                this.$containerhero.find(".posthero").each(function (index, element) {
                                    _this.$containerhero.fadeOut(300, function () {
                                        if ($(element).hasClass(targetFilter)) {
                                            $(element).show();
                                        } else {
                                            $(element).hide();
                                        }
                                        _this.$containerhero.fadeIn();
                                    });
                                });
                            }
                        } }]);return FilterGallery;}();


                    var fliterGallery = new FilterGallery();
                })(jQuery);

                jQuery(document).ready(function($) {
                    // Scroll to the desired section on click
                    // Make sure to add the `data-scroll` attribute to your `<a>`          tag.
                    // Example:
                    // `<a data-scroll href="#my-section">My Section</a>` will scroll to an element with the id of 'my-section'.

                    function scrollToSection(event) {
                        if ( window.location.pathname == '/' ){
                            event.preventDefault();
                            var $section = $($(this).attr('href'));
                            $('html, body').animate({
                                scrollTop: $section.offset().top-70
                            }, 500);
                        }
                    }
                    $('[data-scroll]').on('click', scrollToSection);
                }(jQuery));

            },
            finalize: function () {
                // JavaScript to be fired on all pages, after page specific JS is fired
            }
        },
        // Home page
        'home': {
            init: function () {
                // JavaScript to be fired on the home page
                /*$(window).scroll(function () {
                    var scroll = $(window).scrollTop();
                    //console.log(scroll);
                    if (scroll >= 50) {
                        //console.log('a');
                        $(".main-header").addClass("main-header-invert");
                    } else {
                        //console.log('a');
                        $(".main-header").removeClass("main-header-invert");
                    }

                });*/
            },
            finalize: function () {
                // JavaScript to be fired on the home page, after the init JS
            }
        },
        // About us page, note the change from about-us to about_us.
        'gallery': {
            init: function () {
                // JavaScript to be fired on the home page
                jQuery(document).ready(function ($) {
                    // Scroll to the desired section on click
                    // Make sure to add the `data-scroll` attribute to your `<a>`          tag.
                    // Example:
                    // `<a data-scroll href="#my-section">My Section</a>` will scroll to an element with the id of 'my-section'.

                    function scrollToSection(event) {
                        if (window.location.pathname == '/') {
                            event.preventDefault();
                            var $section = $($(this).attr('href'));
                            $('html, body').animate({
                                scrollTop: $section.offset().top - 70
                            }, 500);
                        }
                    }

                    $('[data-scroll]').on('click', scrollToSection);
                }(jQuery));
            },

        },

        'about_us': {
            init: function () {
                // JavaScript to be fired on the about us page
            }
        }

    };

    // The routing fires all common scripts, followed by the page specific scripts.
    // Add additional events for more control over timing e.g. a finalize event
    var UTIL = {
        fire: function(func, funcname, args) {
            var fire;
            var namespace = Sage;
            funcname = (funcname === undefined) ? 'init' : funcname;
            fire = func !== '';
            fire = fire && namespace[func];
            fire = fire && typeof namespace[func][funcname] === 'function';

            if (fire) {
                namespace[func][funcname](args);
            }
        },
        loadEvents: function() {
            // Fire common init JS
            UTIL.fire('common');

            // Fire page-specific init JS, and then finalize JS
            $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
                UTIL.fire(classnm);
                UTIL.fire(classnm, 'finalize');
            });

            // Fire common finalize JS
            UTIL.fire('common', 'finalize');
        }
    };

    // Load Events
    $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
