<header class="main-header <?php echo (!is_front_page()) ? 'main-header-invert':''; ?>">
    <nav class="navbar navbar-main">
        <button class="navbar-toggler" type="button" >
            <span class="navbar-toggler-icon"></span>
        </button>

        <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>">
            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/logo.png" alt="">
            <img class="navbar-brand-o" src="<?php echo get_template_directory_uri(); ?>/dist/images/logo-default.png" alt="">
        </a>

        <?php
        if (has_nav_menu('primary_navigation')) :
            wp_nav_menu([
                'theme_location' => 'primary_navigation',
                'container' => false,
                'depth'     => 2, // 1 = no dropdowns, 2 = with dropdowns.
                'menu_class' => 'navbar-nav navbar-main-menu',
                'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                'walker' => new WP_Bootstrap_Navwalker()
            ]);
        endif;
        ?>

        <?php $special_link = get_field('special_link','option'); ?>
        <?php if (!empty($special_link['url'])): ?>
            <a href="<?php echo $special_link['url']; ?>" class="btn btn-outline-light btn-action-call"><i class="fa fa-phone"></i><?php echo !empty($special_link['title']) ? $special_link['title']: 'Reach Us!'; ?></a>
        <?php endif; ?>
    </nav>

</header>
