<?php $footer_hero = get_sub_field('footer_hero'); ?>
<div class="footer-hero">
    <div class="container">
        <?php echo $footer_hero['content']; ?>
        <?php if (!empty($footer_hero['link']['url'])): ?>
            <a href="<?php echo $footer_hero['link']['url']?>" class="btn btn-outline-light"><?php echo !empty($footer_hero['link']['title']) ? $footer_hero['link']['title']: 'Contact Now'?></a>
        <?php endif;?>
    </div>
</div>