<?php $d_gallery = get_sub_field('3d_gallery'); ?>
<div class="section-3d-gallery" <?php echo (!empty($d_gallery['section_id'])) ? 'id="'. $d_gallery['section_id'] .'"':'';?>>
    <div class="container">
        <?php if(!empty($d_gallery['content'])):?>
            <div class="d-gallery-intro">
                <?php echo $d_gallery['content']; ?>
            </div>
        <?php endif; ?>
        <?php if ($d_gallery_items = $d_gallery['items']):?>
            <div class="card-deck card-deck-3d-gallery">
                <?php foreach ($d_gallery_items as $item): ?>
                    <div class="card text-white">
                        <img class="card-img" src="<?php echo $item['image']['url']; ?>" alt="<?php echo $item['image']['alt']; ?>">
                        <div class="card-img-overlay">
                            <h5 class="card-title"><?php echo $item['title']; ?></h5>

                            <?php if (!empty($item['link']['url'])): ?>
                                <p class="card-text"><a data-fancybox data-type="iframe" href="javascript:;" data-src="<?php echo $item['link']['url']?>"><?php echo !empty($item['link']['title']) ? $item['link']['title']:'View'?></a></p>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        <?php endif; ?>
    </div>
</div>