<?php $choose_your_plan = get_sub_field('choose_your_plan'); ?>
<div class="section-pricing ptb-60" <?php echo !empty($choose_your_plan['background_color']) ? 'style="background-color:'.$choose_your_plan['background_color'].'"':''; ?>>
    <div class="container">
        <div class="section-centered-intro text-center">
            <?php echo $choose_your_plan['intro']; ?>
        </div>
        <?php if ($items = $choose_your_plan['item']): ?>
            <div class="card-deck card-deck-pricing<?php echo $choose_your_plan['layout'] ? ' card-deck-layout-'.$choose_your_plan['layout'].'':''; ?> mb-3 text-center">
                <?php foreach ($items as $item): ?>
                    <div class="card<?php echo $item['featured'] ? ' card-featured':''; ?> shadow-sm">
                        <div class="card-header">
                            <h4 class="my-0 font-weight-normal"><?php echo $item['title']; ?></h4>
                        </div>
                        <div class="card-body">
                            <h1 class="card-title pricing-card-title"><?php echo $item['value']; ?></h1>
                            <?php echo !empty($item['subtitle']) ? '<span>'.$item['subtitle'].'</span>': '<span class="text-placeholder">Total Cost</span>'; ?>
                            <?php $lists = $item['list']; ?>
                            <?php if ($lists): ?>
                                <ul class="list-unstyled mt-3 mb-4">
                                    <?php foreach ($lists as $list): ?>
                                        <li><?php echo $list['item']; ?></li>
                                    <?php endforeach;?>
                                </ul>
                            <?php endif; ?>
                            <a href="<?php echo $item['link']['url']; ?>" class="btn btn-lg btn-block btn-primary"><?php echo !empty($item['link']['title']) ? $item['link']['title'] :'Get Started'; ?></a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
