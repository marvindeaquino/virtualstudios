<?php $subpage_banner = get_sub_field('subpage_banner'); ?>
<div class="section-subpage-banner ptb-80" <?php echo (!empty($subpage_banner['background_color'])) ? 'style="background-color:'.$subpage_banner['background_color'].';""':'';?>>
    <div class="container">
        <h1 class="text-white text-center"><?php echo $subpage_banner['title']?></h1>
    </div>
</div>
