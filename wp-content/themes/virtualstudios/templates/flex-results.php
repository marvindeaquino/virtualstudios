<?php $results = get_sub_field('results')?>
<?php $items = $results['item']; ?>
<div class="section-default section-results ptb-40" style="background: <?php echo $results['background_color']; ?>">
    <div class="container">
        <?php if ($items): ?>
            <div class="row row-results">
                <?php foreach ($items as $item): ?>
                    <div class="col-md-4">
                        <i class="fa fa-<?php echo $item['icon']; ?>"></i>
                        <span class="percent"><?php echo $item['percentage']; ?></span>
                        <?php echo $item['content']; ?>
                    </div>
                <?php endforeach;?>
            </div>
        <?php endif; ?>
    </div>
</div>
