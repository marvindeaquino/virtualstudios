<?php $section_client = get_sub_field('section_client'); ?>
<div class="section-client-logo ptb-80">
    <div class="container">
        <div class="section-heading">
            <h2 class="h1"><?php echo $section_client['title']; ?></h2>
        </div>

        <div class="row">
            <?php if ($client_logos = $section_client['client_logo']): ?>
                <?php foreach ($client_logos as $client_logo): ?>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="client-logo">
                            <?php if (!empty($client_logo['link']['url'])): ?>
                                <a href="<?php echo $client_logo['link']['url']; ?>"><img class="img-fluid" src="<?php echo $client_logo['image']['url']; ?>" alt=""></a>
                            <?php else: ?>
                                <a><img class="img-fluid" src="<?php echo $client_logo['image']['url']; ?>" alt=""></a>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach;?>
            <?php endif; ?>
        </div>
    </div>
</div>
