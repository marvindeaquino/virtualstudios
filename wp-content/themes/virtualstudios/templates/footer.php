<?php /*dynamic_sidebar('sidebar-footer'); */?>
<?php $footer = get_field('footer','option'); ?>
<?php $footer_brand = get_field('footer_brand','option'); ?>
<?php $request_quote = get_field('request_quote','option');?>
<?php if (!empty($request_quote['content']) || !empty($request_quote['link']['url'])):?>
<div class="footer-hero">
    <div class="container">
        <?php echo $request_quote['content']?>
        <?php if (!empty($request_quote['link']['url'])): ?>
            <a href="<?php echo $request_quote['link']['url']?>" class="btn btn-outline-light"><?php echo !empty($request_quote['link']['title']) ? $request_quote['link']['title']: 'Contact Now'?></a>
        <?php endif;?>
    </div>
</div>
<?php endif;?>
<div class="last-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="footer-brand">
                    <a href="<?php echo $footer_brand['link']['url']?>"><img class="navbar-brand-o" src="<?php echo $footer_brand['image']['url'] ?>" alt="<?php echo $footer_brand['image']['alt'] ?>"></a>
                </div>
                <div class="content">
                    <?php echo $footer_brand['content']; ?>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="row">
                    <div class="col-md-4 col-lg-3">
                        <h4>Links</h4>
                        <?php
                            if (has_nav_menu('footer_navigation')) :
                                wp_nav_menu([
                                    'theme_location' => 'footer_navigation',
                                    'container' => false,
                                    'menu_class' => '',

                                ]);
                            endif;
                        ?>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <h4>Contact Us</h4>
                        <?php if ($contact_details = get_field('contact_details','option')): ?>
                            <ul>
                                <?php foreach ($contact_details as $keys => $contact_detail):?>
                                    <li><a <?php echo $contact_detail['link']['url'] == '#' ? '': 'href="'.$contact_detail['link']['url'].'"'; echo !empty($contact_detail['link']['target']) ? 'target"'.$contact_detail['link']['target'].'"':''; ?>><?php echo $contact_detail['link']['title']?></a></li>
                                <?php endforeach;?>
                            </ul>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <h4>Social Media</h4>
                        <?php if ($sml = get_field('social_media_links','option')):?>
                            <ul class="social-media">
                                <?php foreach ($sml as $sml_item):?>
                                    <li><a href="<?php echo $sml_item['link']['url']?>"><i class="fa fa-<?php echo $sml_item['icon']; ?>"></i></a></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="copyright-footer text-center">
    <div class="container">
        <p><?php the_field('copyrights','option')?></p>
    </div>
</div>