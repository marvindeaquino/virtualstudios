<aside class="navbar-mobile">
  <div class="navbar-left">
    <button class="navbar-toggler" type="button" >
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>">

      <img src="<?php echo get_template_directory_uri(); ?>/dist/images/logo-default.png" alt="">
    </a>
  </div>

  <div class="mobile-nav">
    <?php
    if (has_nav_menu('primary_navigation')) :
      wp_nav_menu([
        'theme_location' => 'primary_navigation',
        'container' => false,
        'depth'     => 2, // 1 = no dropdowns, 2 = with dropdowns.
        'menu_class' => 'navbar-inline'
      ]);
    endif;
    ?>
  </div>
</aside>

<span class="mobile-content-shadow"></span>
