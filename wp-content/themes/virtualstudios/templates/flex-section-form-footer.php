<?php $section_form_footer = get_sub_field('section_form_footer'); ?>

<div class="section-form-footer" id="footer-contact-form">
    <div class="container">
        <div class="form-footer">
            <h3 class="form-heading"><?php echo $section_form_footer['title']; ?></h3>
            <?php echo $section_form_footer['content']; ?>
        </div>
    </div>
</div>