<?php $section_services_list = get_sub_field('section_services_list'); ?>
<div class="section-services-list ptb-80" <?php echo !empty($section_services_list['background_color']) ? 'style="background-color: '.$section_services_list['background_color'].'"':''; ?>>
    <div class="container">
        <?php if (!empty($section_services_list['content'])): ?>
            <div class="section-copy pb-1">
                <?php echo $section_services_list['content']; ?>
            </div>
        <?php endif;?>
        <?php if ($checklists = $section_services_list['checklist']): ?>
        <div class="row row-checklist">
            <?php foreach ($checklists as $checklist):?>
                <div class="col-md-6">
                    <div class="checklist">
                        <div class="checklist-icon"><i class="<?php echo !empty($checklist['icon']) ? 'iconmoon-'.$checklist['icon']:'fa fa-check-circle-o';?>"></i></div>
                        <div class="checklist-details">
                            <?php echo $checklist['checklist_details']; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
        <?php endif; ?>
    </div>
</div>
