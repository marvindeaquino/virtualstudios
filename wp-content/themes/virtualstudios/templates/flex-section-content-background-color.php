<?php $scbc = get_sub_field('section_content_background_color'); ?>
<div class="section-content-background-color ptb-80" <?php echo !empty($scbc['background_color']) ? 'style="background-color:'.$scbc['background_color'].';"' : ''; ?>>
    <div class="container text-center">
        <?php echo $scbc['content']; ?>
    </div>
</div>
