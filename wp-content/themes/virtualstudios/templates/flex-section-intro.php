<?php $section_intro = get_sub_field('section_intro')?>
<div class="section-intro">
    <div class="container">
        <div class="section-copy">
            <?php echo $section_intro['content']; ?>
            <?php if (!empty($section_intro['link']['url'])): ?>
                <a <?php echo (strpos($section_intro['link']['url'], '#') !== false) ? 'data-scroll ':''?>href="<?php echo $section_intro['link']['url'];?>" class="btn btn-outline-primary"><?php echo !empty($section_intro['link']['title']) ? $section_intro['link']['title']:'Apply in your Industry';?></a>
            <?php endif;?>
        </div>
    </div>
</div>