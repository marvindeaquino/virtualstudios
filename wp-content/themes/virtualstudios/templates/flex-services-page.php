<?php $virtual_3d_tours = get_sub_field('services_page_virtual_3d_tours');?>
<div class="section-default section-services-page ptb-40">
    <div class="container">
        <div class="row row-virtual-tours">
            <div class="col-md-6">
                <?php echo $virtual_3d_tours['content']?>
                <?php $cloned_links = $virtual_3d_tours['link_repeater']?>
                <?php if ($cloned_links): ?>
                    <?php foreach ($cloned_links as $cloned_link): ?>
                        <?php if (!empty($cloned_link['link']['url'])):?>
                            <?php if (($cloned_link['fancybox'] && $cloned_link['virtual_3d'])): ?>
                                <a class="btn btn-primary" data-fancybox data-type="iframe" href="javascript:;" data-src="<?php echo $cloned_link['link']['url']?>" >
                                    <?php echo !empty($cloned_link['link']['title']) ? $cloned_link['link']['title']: 'Learn More'; ?><?php echo !empty($cloned_link['cloned_icon']) ? '<i class="fa fa-'.$cloned_link['cloned_icon'].'" aria-hidden="true"></i>':'';?>
                                </a>
                            <?php elseif ($cloned_link['fancybox'] && !$cloned_link['virtual_3d']):?>
                                <a class="btn btn-primary" data-fancybox<?php echo $cloned_link['fancybox'] ? '="'.$cloned_link['fancybox_label'].'"':''; ?> href="<?php echo $cloned_link['link']['url']?>">
                                    <?php echo !empty($cloned_link['link']['title']) ? $cloned_link['link']['title']: 'Learn More'; ?><?php echo !empty($cloned_link['cloned_icon']) ? '<i class="fa fa-'.$cloned_link['cloned_icon'].'" aria-hidden="true"></i>':'';?>
                                </a>
                            <?php else:?>
                                <a class="btn btn-primary" href="<?php echo $cloned_link['link']['url']?>" <?php echo empty($cloned_link['link']['target']) ? '' : 'target="'.$cloned_link['link']['target'].'"'?>>
                                    <?php echo !empty($cloned_link['link']['title']) ? $cloned_link['link']['title']: 'Learn More'; ?><?php echo !empty($cloned_link['cloned_icon']) ? '<i class="fa fa-'.$cloned_link['cloned_icon'].'" aria-hidden="true"></i>':'';?>
                                </a>
                            <?php endif;?>
                        <?php endif; ?>
                    <?php endforeach;?>
                <?php endif; ?>
            </div>
            <div class="col-md-6">
                <?php if ($virtual_3d_tours['image_inside_link']): ?>
                    <?php if (!empty($virtual_3d_tours['fancybox'])): ?>

                        <?php if (($virtual_3d_tours['fancybox'] && $virtual_3d_tours['virtual_3d'])): ?>
                            <a class="gallery" data-fancybox data-type="iframe" href="javascript:;" data-src="<?php echo $virtual_3d_tours['link']['url']?>" >
                                <img class="img-fluid" src="<?php echo $virtual_3d_tours['image']['url']; ?>">
                                <i class="fa fa-<?php echo $virtual_3d_tours['icon']; ?>"></i>
                            </a>
                        <?php elseif ($virtual_3d_tours['fancybox'] && !$virtual_3d_tours['virtual_3d']):?>
                            <a class="gallery" data-fancybox<?php echo $virtual_3d_tours['fancybox'] ? '="'.$virtual_3d_tours['fancybox_label'].'"':''; ?> href="<?php echo $virtual_3d_tours['link']['url']?>">
                                <img class="img-fluid" src="<?php echo $virtual_3d_tours['image']['url']; ?>">
                                <i class="fa fa-<?php echo $virtual_3d_tours['icon']; ?>"></i>                            </a>
                        <?php else:?>
                            <a class="gallery" href="<?php echo $virtual_3d_tours['link']['url']?>" <?php echo empty($virtual_3d_tours['link']['target']) ? '' : 'target="'.$virtual_3d_tours['link']['target'].'"'?>>
                                <img class="img-fluid" src="<?php echo $virtual_3d_tours['image']['url']; ?>">
                                <i class="fa fa-<?php echo $virtual_3d_tours['icon']; ?>"></i>
                            </a>
                        <?php endif;?>
                    <?php endif;?>
                <?php else: ?>
                    <img class="img-fluid" src="<?php echo $virtual_3d_tours['image']['url']; ?>">
                <?php endif;?>
            </div>
        </div>
    </div>
</div>
