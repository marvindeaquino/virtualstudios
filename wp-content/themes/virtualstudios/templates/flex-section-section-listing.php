<?php $section_section_listing = get_sub_field('section_section_listing'); ?>
<div class="section-section-listing">
    <div class="container">
        <div class="section-copy">
            <?php echo $section_section_listing['content']?>
        </div>
        <?php if ($post_industries = $section_section_listing['post_industry']): ?>
            <div class="row">
                <?php foreach ($post_industries as $post_industry): ?>
                    <div class="col-md-4">
                        <div class="post-industry">
                            <div class="post-img" style="background-image:url('<?php echo $post_industry['image']['url']; ?>');"></div>
                            <div class="post-body">
                                <?php echo $post_industry['content']; ?>
                                <?php if ($lists = $post_industry['list']): ?>
                                    <ul class="list">
                                        <?php foreach ($lists as $list): ?>
                                            <?php if (!empty($list['image']['url'])):?>
                                                <li>
                                                    <a href="<?php echo $list['link']['url']?>">
                                                        <img src="<?php echo $list['image']['url'];?>" alt="<?php echo $list['image']['alt'];?>">
                                                        <?php echo $list['link']['title']?>
                                                    </a>
                                                </li>
                                                <?php else:?>
                                                <li><a href="<?php echo $list['link']['url']?>"><?php echo $list['link']['title']?></a></li>
                                            <?php endif;?>

                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif;?>
                            </div>
                            <div class="post-footer">
                                <?php if ($post_industry['link']['url']): ?>
                                    <a class="btn-link" href="<?php echo $post_industry['link']['url']; ?>"><?php echo ($post_industry['link']['title']) ? $post_industry['link']['title']: 'View More '; ?><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        <?php endif; ?>
    </div>
</div>