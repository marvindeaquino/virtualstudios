<?php $gallery_page = get_sub_field('gallery_page');?>
<?php if ($gallery_page): ?>

    <div class="section-filter">
        <div class="container">
            <ul class="filtermenu">
                <?php $category  = array('real-state' =>'Real State','travel-n-hospitality'=>'Travel & Hospitality','commercial'=>'Commercial','real-state'=>'Real State','event-spaces'=>'Event Spaces','architecture'=>'Architecture'); ?>
                <li data-filter="all">All</li>
                <?php foreach ($category as $value => $label):?>
                    <li data-filter="<?php echo $value; ?>"><?php echo $label; ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>

  <?php if ($hero_intro  =  get_sub_field('description')): ?>
    <div class="section-hero-intro">
      <div class="container">
        <div class="section-listing-hero">
          <div class="row row-listing-hero">
            <?php foreach ($hero_intro as $item):?>
              <div class="col-sm-12 posthero posthero-sub <?php echo $item['cloned_subtitle_subtitle']['value']; ?>" style="display: none;">
                <?php echo $item['content'];?>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>
  <?php endif; ?>
    <div class="section-gallery-page">
        <div class="container">
            <div class="section-listing">
                <div class="row">
                    <?php foreach ($gallery_page as $gp_item):?>
                        <div class="col-sm-12 col-md-6 col-lg-4 post <?php echo $gp_item['subtitle']['value']; ?>">
                            <div class="post-project">
                                <div class="post-img"style="background-image:url('<?php echo $gp_item['image']['url']?>')">
                                    <span class="post-ribbon"><?php echo $gp_item['subtitle']['label']; ?></span>
                                    <span class="post-copy">
                                        <?php echo $gp_item['content']; ?>
                                        <?php if (!empty($gp_item['link']['url'])): ?>
                                            <a href="<?php echo $gp_item['link']['url']; ?>" class="btn btn-primary"><?php echo !empty($gp_item['link']['tile']) ? $gp_item['link']['tile'] : 'Virtual 3D-Tour'; ?></a>
                                        <?php endif; ?>
                                    </span>
                                </div>
                                <h2><?php echo $gp_item['title']; ?></h2>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
