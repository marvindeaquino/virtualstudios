<?php

if( have_rows('section_blocks') ):

    while ( have_rows('section_blocks') ) : the_row();

        if( get_row_layout() == 'home_banner' ):

            get_template_part('templates/flex', 'home-banner');

        elseif( get_row_layout() == 'results' ):

            get_template_part('templates/flex', 'results');

        elseif( get_row_layout() == 'virtual_3d_tours' ):

            get_template_part('templates/flex', 'virtual-tours');

        elseif( get_row_layout() == 'industries' ):

            get_template_part('templates/flex', 'industries');

        elseif( get_row_layout() == 'gallery' ):

            get_template_part('templates/flex', 'gallery');

        elseif( get_row_layout() == 'request_quote' ):

            get_template_part('templates/flex', 'request-quote');

        elseif( get_row_layout() == 'simple_page' ):

            get_template_part('templates/flex', 'simple-page');

        elseif( get_row_layout() == '3d_gallery' ):

            get_template_part('templates/flex', '3d-gallery');

        elseif( get_row_layout() == 'project' ):

            get_template_part('templates/flex', 'project');

        elseif( get_row_layout() == 'about_us' ):

            get_template_part('templates/flex', 'about');


        elseif( get_row_layout() == 'home_3d_gallery' ):

            get_template_part('templates/flex', 'home-3d-gallery');

        elseif( get_row_layout() == 'gallery_page' ):

            get_template_part('templates/flex', 'gallery-page');

        elseif( get_row_layout() == 'services_page' ):

            get_template_part('templates/flex', 'services-page');

        elseif( get_row_layout() == 'three_column_items_with_image' ):

            get_template_part('templates/flex', '3-column-items-with-image');

        elseif( get_row_layout() == 'section_content_background_color' ):

            get_template_part('templates/flex', 'section-content-background-color');

        elseif( get_row_layout() == 'photo_collection' ):

            get_template_part('templates/flex', 'photo-collection');

        elseif( get_row_layout() == 'subpage_banner' ):

            get_template_part('templates/flex', 'subpage-banner');

        elseif( get_row_layout() == 'image_left_content_right' ):

            get_template_part('templates/flex', 'image-l-content-r');

        elseif( get_row_layout() == 'map' ):

            get_template_part('templates/flex', 'map');

        elseif( get_row_layout() == 'choose_your_plan' ):

            get_template_part('templates/flex', 'pricing');

        elseif( get_row_layout() == 'section_intro' ):

            get_template_part('templates/flex', 'section-intro');

        elseif( get_row_layout() == 'section_section_listing' ):

            get_template_part('templates/flex', 'section-section-listing');

        elseif( get_row_layout() == 'section_column_post' ):

            get_template_part('templates/flex', 'section-column-post');

        elseif( get_row_layout() == 'section_form_footer' ):

            get_template_part('templates/flex', 'section-form-footer');

        elseif( get_row_layout() == 'footer_hero' ):

            get_template_part('templates/flex', 'footer-hero');

        elseif( get_row_layout() == 'section_services_list' ):

            get_template_part('templates/flex', 'section-services-list');

        elseif( get_row_layout() == 'section_client' ):

            get_template_part('templates/flex', 'client-logo');

        elseif( get_row_layout() == 'industry_benefits​' ):

            get_template_part('templates/flex', 'industry-benefits');
        
        endif;

    endwhile;

endif;
