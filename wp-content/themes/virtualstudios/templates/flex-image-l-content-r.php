<?php $image_l_content_r = get_sub_field('clone_v3d_virtual_3d_tours');?>
<div class="section-default section-image-left-content-right ptb-40" <?php echo !empty($image_l_content_r['background_color']) ? 'style="background-color:'.$image_l_content_r['background_color'].';"':'';?> >
    <div class="container">
      <?php if($image_l_content_r['content']): ?>
        <div class="row row-virtual-tours">
            <div class="col-md-6">
                <div class="media-holder" style="background-image:url(<?php echo $image_l_content_r['image']['url']; ?>)">
                  <?php if (!empty($image_l_content_r['link']['url'])): ?>
                    <?php if (($image_l_content_r['fancybox'] && $image_l_content_r['virtual_3d'])): ?>
                      <a class="circle-play" data-fancybox data-type="iframe" href="javascript:;" data-src="<?php echo $image_l_content_r['link']['url']?>" >
                        <?php echo !empty($image_l_content_r['icon']) ? '<i class="fa fa-'.$image_l_content_r['icon'].'" aria-hidden="true"></i>':'';?>
                      </a>
                    <?php elseif ($cloned_link['fancybox'] && !$cloned_link['virtual_3d']):?>
                      <a class="circle-play" data-fancybox<?php echo $cloned_link['fancybox'] ? '="'.$image_l_content_r['fancybox_label'].'"':''; ?> href="<?php echo $image_l_content_r['link']['url']?>">
                        <?php echo !empty($image_l_content_r['icon']) ? '<i class="fa fa-'.$image_l_content_r['icon'].'" aria-hidden="true"></i>':'';?>
                      </a>
                    <?php else:?>
                      <a class="circle-play" href="<?php echo $image_l_content_r['link']['url']?>">
                        <?php echo !empty($image_l_content_r['icon']) ? '<i class="fa fa-'.$image_l_content_r['icon'].'" aria-hidden="true"></i>':'';?>
                      </a>
                    <?php endif;?>
                  <?php endif;?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-copy">
                  <?php echo $image_l_content_r['content']; ?>
                  <?php $cloned_links = $image_l_content_r['link_repeater']?>
                  <?php if ($cloned_links): ?>
                    <?php foreach ($cloned_links as $cloned_link): ?>
                      <?php if (!empty($cloned_link['link']['url'])):?>
                        <?php if (($cloned_link['fancybox'] && $cloned_link['virtual_3d'])): ?>
                          <a class="btn btn-primary" data-fancybox data-type="iframe" href="javascript:;" data-src="<?php echo $cloned_link['link']['url']?>" >
                            <?php echo !empty($cloned_link['link']['title']) ? $cloned_link['link']['title']: 'Learn More'; ?><?php echo !empty($cloned_link['cloned_icon']) ? '<i class="fa fa-'.$cloned_link['cloned_icon'].'" aria-hidden="true"></i>':'';?>
                          </a>
                        <?php elseif ($cloned_link['fancybox'] && !$cloned_link['virtual_3d']):?>
                          <a class="btn btn-primary" data-fancybox<?php echo $cloned_link['fancybox'] ? '="'.$cloned_link['fancybox_label'].'"':''; ?> href="<?php echo $cloned_link['link']['url']?>">
                            <?php echo !empty($cloned_link['link']['title']) ? $cloned_link['link']['title']: 'Learn More'; ?><?php echo !empty($cloned_link['cloned_icon']) ? '<i class="fa fa-'.$cloned_link['cloned_icon'].'" aria-hidden="true"></i>':'';?>
                          </a>
                        <?php else:?>
                          <a class="btn btn-primary" href="<?php echo $cloned_link['link']['url']?>" <?php echo empty($cloned_link['link']['target']) ? '' : 'target="'.$cloned_link['link']['target'].'"'?>>
                            <?php echo !empty($cloned_link['link']['title']) ? $cloned_link['link']['title']: 'Learn More'; ?><?php echo !empty($cloned_link['cloned_icon']) ? '<i class="fa fa-'.$cloned_link['cloned_icon'].'" aria-hidden="true"></i>':'';?>
                          </a>
                        <?php endif;?>
                      <?php endif; ?>
                    <?php endforeach;?>
                  <?php endif; ?>
                </div>
            </div>
        </div>
      <?php else:?>
        <div class="media-holder lg" style="background-image:url(<?php echo $image_l_content_r['image']['url']; ?>)">
          <?php if (!empty($image_l_content_r['link']['url'])): ?>
            <?php if (($image_l_content_r['fancybox'] && $image_l_content_r['virtual_3d'])): ?>
              <a class="circle-play" data-fancybox data-type="iframe" href="javascript:;" data-src="<?php echo $image_l_content_r['link']['url']?>" >
                <?php echo !empty($image_l_content_r['icon']) ? '<i class="fa fa-'.$image_l_content_r['icon'].'" aria-hidden="true"></i>':'';?>
              </a>
            <?php elseif ($cloned_link['fancybox'] && !$cloned_link['virtual_3d']):?>
              <a class="circle-play" data-fancybox<?php echo $cloned_link['fancybox'] ? '="'.$image_l_content_r['fancybox_label'].'"':''; ?> href="<?php echo $image_l_content_r['link']['url']?>">
                <?php echo !empty($image_l_content_r['icon']) ? '<i class="fa fa-'.$image_l_content_r['icon'].'" aria-hidden="true"></i>':'';?>
              </a>
            <?php else:?>
              <a class="circle-play" href="<?php echo $image_l_content_r['link']['url']?>">
                <?php echo !empty($image_l_content_r['icon']) ? '<i class="fa fa-'.$image_l_content_r['icon'].'" aria-hidden="true"></i>':'';?>
              </a>
            <?php endif;?>
          <?php endif;?>
        </div>
      <?php endif; ?>
    </div>
</div>
