<?php $about = get_sub_field('about_us'); ?>
<div class="section-about-us ptb-80">
    <div class="row row-about-us">
        <div class="col-sm-7 full-col-left-padding full-col-right-padding">
            <?php echo $about['content']?>
        </div>
        <div class="col-sm-5 shadow-large-black revert-classes-holder" style="background-image: url('<?php echo $about['image']['url']?>')"></div>
    </div>
</div>
