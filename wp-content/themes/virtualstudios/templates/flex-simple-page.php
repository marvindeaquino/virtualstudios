<?php $simple_page = get_sub_field('simple_page'); ?>
<div class="section-simple-page" <?php echo !empty( $simple_page['background_color']) ? "style='background-color:". $simple_page['background_color'].";'    ":""?>>
    <div class="container">
        <?php echo $simple_page['content']; ?>
    </div>
</div>
