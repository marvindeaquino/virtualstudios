<div class="section-column-post">
  <div class="container">
    <div class="post-simple-holder">
      <div class="post-simple">
        <div class="post-img" style="background-image:url(https://realitycaptureexperts.com/wp-content/uploads/2018/03/Mattertags.png)">
          <h3>​Highlight Features</h3>
        </div>

      </div>
      <div class="post-simple">
        <div class="post-img" style="background-image:url('https://realitycaptureexperts.com/wp-content/uploads/2018/02/Desktop_Mobile_VR.png')">
          <h3>Compatible on Any Device</h3>
        </div>

      </div>
      <div class="post-simple">
        <div class="post-img" style="background-image:url('https://realitycaptureexperts.com/wp-content/uploads/2018/03/18199402_762626453905509_977845320230216821_n.jpg')">
          <h3>​Interactive Dollhouse View</h3>
        </div>

      </div>
      <div class="post-simple">
        <div class="post-img" style="background-image:url('https://realitycaptureexperts.com/wp-content/uploads/2018/03/kingstable11.png')">
          <h3>​Floor Plans</h3>
        </div>

      </div>
    </div>
  </div>
</div>


<div class="section-form-footer" id="footer-contact-form">
  <div class="container">


    <div class="form-footer">

      <h3 class="form-heading">Contact Virtual Studios</h3>
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label for="">First name</label>
            <input type="text" class="form-control">
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label for="">Last name</label>
            <input type="text" class="form-control">
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label for="">Email</label>
            <input type="text" class="form-control">
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label for="">Phone</label>
            <input type="text" class="form-control">
          </div>
        </div>
      </div>


      <div class="form-group">
        <label for="">Messaage</label>
        <textarea name="" id="" cols="30" rows="8" class="form-control"></textarea>
      </div>

      <div class="form-actions">
        <button class="btn btn-primary">Submit</button>
      </div>
    </div>
  </div>
</div>
