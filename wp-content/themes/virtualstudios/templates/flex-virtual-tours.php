<?php $virtual_3d_tours = get_sub_field('virtual_3d_tours');?>
<div class="section-tour">
    <div class="container">
        <div class="section-copy">
            <?php echo $virtual_3d_tours['content']?>
            <?php $cloned_links = $virtual_3d_tours['link_repeater']?>
            <?php if ($cloned_links): ?>
                <?php foreach ($cloned_links as $cloned_link): ?>
                    <?php if (!empty($cloned_link['link']['url'])):?>
                        <?php if (($cloned_link['fancybox'] && $cloned_link['virtual_3d'])): ?>
                            <a class="btn btn-primary" data-fancybox data-type="iframe" href="javascript:;" data-src="<?php echo $cloned_link['link']['url']?>" >
                                <?php echo !empty($cloned_link['link']['title']) ? $cloned_link['link']['title']: 'Learn More'; ?><?php echo !empty($cloned_link['cloned_icon']) ? '<i class="fa fa-'.$cloned_link['cloned_icon'].'" aria-hidden="true"></i>':'';?>
                            </a>
                        <?php elseif ($cloned_link['fancybox'] && !$cloned_link['virtual_3d']):?>
                            <a class="btn btn-primary" data-fancybox<?php echo $cloned_link['fancybox'] ? '="'.$cloned_link['fancybox_label'].'"':''; ?> href="<?php echo $cloned_link['link']['url']?>">
                                <?php echo !empty($cloned_link['link']['title']) ? $cloned_link['link']['title']: 'Learn More'; ?><?php echo !empty($cloned_link['cloned_icon']) ? '<i class="fa fa-'.$cloned_link['cloned_icon'].'" aria-hidden="true"></i>':'';?>
                            </a>
                        <?php else:?>
                            <a class="btn btn-primary" href="<?php echo $cloned_link['link']['url']?>" <?php echo empty($cloned_link['link']['target']) ? '' : 'target="'.$cloned_link['link']['target'].'"'?>>
                                <?php echo !empty($cloned_link['link']['title']) ? $cloned_link['link']['title']: 'Learn More'; ?><?php echo !empty($cloned_link['cloned_icon']) ? '<i class="fa fa-'.$cloned_link['cloned_icon'].'" aria-hidden="true"></i>':'';?>
                            </a>
                        <?php endif;?>
                    <?php else: ?>
                        <img class="img-fluid" src="<?php echo $cloned_link['image']['url']; ?>">
                    <?php endif; ?>
                <?php endforeach;?>
            <?php endif; ?>
        </div>
        <div class="section-img" style="background-image:url('<?php echo $virtual_3d_tours['image']['url']; ?>')">
            <?php if (!empty($virtual_3d_tours['link']['url'])): ?>
                <?php if (($virtual_3d_tours['fancybox'] && $virtual_3d_tours['virtual_3d'])): ?>
                    <a class="circle-play" data-fancybox data-type="iframe" href="javascript:;" data-src="<?php echo $virtual_3d_tours['link']['url']?>" >
                        <?php echo !empty($virtual_3d_tours['icon']) ? '<i class="fa fa-'.$virtual_3d_tours['icon'].'" aria-hidden="true"></i>':'';?>
                    </a>
                <?php elseif ($cloned_link['fancybox'] && !$cloned_link['virtual_3d']):?>
                    <a class="circle-play" data-fancybox<?php echo $cloned_link['fancybox'] ? '="'.$virtual_3d_tours['fancybox_label'].'"':''; ?> href="<?php echo $virtual_3d_tours['link']['url']?>">
                        <?php echo !empty($virtual_3d_tours['icon']) ? '<i class="fa fa-'.$virtual_3d_tours['icon'].'" aria-hidden="true"></i>':'';?>
                    </a>
                <?php else:?>
                    <a class="circle-play" href="<?php echo $virtual_3d_tours['link']['url']?>">
                        <?php echo !empty($virtual_3d_tours['icon']) ? '<i class="fa fa-'.$virtual_3d_tours['icon'].'" aria-hidden="true"></i>':'';?>
                    </a>
                <?php endif;?>
            <?php endif;?>
        </div>
    </div>
</div>