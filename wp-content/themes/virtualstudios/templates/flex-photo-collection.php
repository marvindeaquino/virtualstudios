<?php $photo_collection = get_sub_field('photo_collection'); ?>
<div class="section-photo-collection">
    <?php if ($photo_collection): ?>
        <div class="row no-gutters row-photo-collertion">
            <?php foreach ($photo_collection as $photo_item):?>
                <div class="col-md-3">
                    <a data-fancybox href="<?php echo $photo_item['image']['url']; ?>">
                        <img class="img-fluid" src="<?php echo $photo_item['image']['url']; ?>" alt="<?php echo $photo_item['image']['alt']; ?>">
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>

