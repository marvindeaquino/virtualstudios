<?php $item = get_sub_field('item'); ?>
<div class="section-request-quote ptb-60">
    <div class="container">
        <?php echo $item['content']; ?>
        <?php if (!empty($item['link']['url'])): ?>
            <a class="btn btn-primary" href="<?php echo $item['link']['url']; ?>"><?php echo empty($item['link']['title']) ? 'Reach us!':$item['link']['title']; ?></a>
        <?php endif;?>
    </div>
</div>