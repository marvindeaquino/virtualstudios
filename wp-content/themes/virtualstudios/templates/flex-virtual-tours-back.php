<?php $virtual_3d_tours = get_sub_field('virtual_3d_tours');?>
<div class="section-default section-virtual-tours ptb-40">
    <div class="container">
        <div class="row row-virtual-tours">
            <div class="col-md-6">
                <?php echo $virtual_3d_tours['content']?>
                <?php $cloned_links = $virtual_3d_tours['cloned_link']?>
                <?php if ($cloned_links): ?>
                    <?php foreach ($cloned_links as $cloned_link): ?>
                        <a href="<?php echo $cloned_link['link']['url']?>" <?php echo ($cloned_link['fancybox'] === true) ? 'data-fancybox="'.$cloned_link['fancybox_label'].'"':''; ?>class="btn btn-primary"><?php echo !empty($cloned_link['link']['title']) ? $cloned_link['link']['title'] : 'Learn more'; ?></a>
                    <?php endforeach;?>
                <?php endif; ?>
            </div>
            <div class="col-md-6">
                <?php if ($virtual_3d_tours['image_inside_link']): ?>
                    <?php if (!empty($virtual_3d_tours['link']['url'])): ?>
                        <a class="gallery" <?php echo $virtual_3d_tours['fancy_box'] ? 'data-fancybox="'.$virtual_3d_tours['fancybox_description'].'"':'';?> href="<?php echo $virtual_3d_tours['link']['url']; ?>">
                            <img class="img-fluid" src="<?php echo $virtual_3d_tours['image']['url']; ?>">
                            <i class="fa fa-play-circle-o"></i>
                        </a>
                    <?php endif;?>
                    <?php else: ?>
                    <img class="img-fluid" src="<?php echo $virtual_3d_tours['image']['url']; ?>">
                <?php endif;?>
            </div>
        </div>
    </div>
</div>
