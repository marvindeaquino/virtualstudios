<?php $home_banner = get_sub_field('home_banner')?>
<?php $items = $home_banner['item']; ?>
<?php if (!empty($home_banner['scroll']['url'])): ?>
    <a data-fancybox data-type="iframe" href="javascript:;" data-src="<?php echo $home_banner['scroll']['url'];?>" class="btn btn-primary float-bottom">Take a tour <i class="fa fa-chevron-right" aria-hidden="true"></i></span></a>
<?php endif;?>

<?php if ($items): ?>
    <?php foreach ($items as $item): ?>
        <div class="section-video">
            <div class="section-inner">
                <div class="section-copy">
                    <?php echo $item['content']?>
                </div>
            </div>
            <?php $results = $item['results']; ?>
            <?php if ($results): ?>
                <div class="section-footer">
                    <div class="item-holder">
                        <?php foreach ($results as $result): ?>
                            <div class="item">
                                <div class="item-copy">
                                    <?php echo $result['content']?>
                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>
            <?php endif; ?>
            <video autoplay="" loop="" muted="" class="bg-video" poster=""> <source src="<?php echo $item['video']; ?>" type="video/mp4"></video>
        </div>
    <?php endforeach;?>
<?php endif; ?>