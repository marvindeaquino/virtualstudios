<?php $industry_benefits = get_sub_field('industry_benefits​'); ?>
<section class="section-default section-industry-benefits" style="background-color: <?php echo $industry_benefits['color']; ?>">
    <div class="container">
        <h2 class="h1"><?php echo $industry_benefits['title']?></h2>
        <img class="img-fluid" src="<?php echo $industry_benefits['image']['url']?>" alt="">
        <?php if ($industry_benefits_lists = $industry_benefits['industry_benefits​_list']): ?>
            <div class="industry-benefit-content">
                <div class="row">
                    <?php foreach ($industry_benefits_lists as $industry_benefits_list):?>
                        <div class="col-md-4">
                            <div class="industry-benefit-list-holder">
                                <?php if ($industry_benefits_list_items = $industry_benefits_list['industry_benefits​_list_item']): ?>
                                    <ul>
                                        <?php foreach ($industry_benefits_list_items as $industry_benefits_list_item): ?>
                                            <li>
                                                <a href="<?php echo $industry_benefits_list_item['link']['url']?>">
                                                    <img src="<?php echo $industry_benefits_list_item['image']['url']?>" alt="" width="35" height="35" class="alignnone wp-image-633" /><?php echo $industry_benefits_list_item['link']['title']?>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif;?>
                            </div>
                        </div>
                    <?php endforeach;?>
                </div>
            </div>
        <?php endif; ?>

        <div class="section-footer">
            <?php if (!empty($industry_benefits['link']['url'])): ?>
                <a href="<?php echo $industry_benefits['link']['url']; ?>" class="btn btn-outline-primary"><?php echo !empty($industry_benefits['link']['title']) ? $industry_benefits['link']['title']:'Apply now'; ?></a>
            <?php endif; ?>
        </div>
    </div>
</section>

