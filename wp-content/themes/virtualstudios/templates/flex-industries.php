<?php $industries = get_sub_field('industries'); ?>
<div class="section-industries">
    <div class="container">
        <div class="section-heading">
            <h2 class="h1"><?php echo $industries['title']?></h2>
        </div>
        <?php $industries_items = $industries['industries_item'];?>
        <?php if ($industries_items):?>
            <div class="post-column">
                <?php foreach ($industries_items as $industries_item): ?>
                    <div class="post-industry">
                        <div class="post-img" style="background-image:url('<?php echo $industries_item['image']['url']; ?>');"></div>
                        <div class="post-body">
                            <?php echo $industries_item['copy']; ?>
                            <?php if (!empty($industries_item['link']['url'])): ?>
                                <a class="btn-link" href="<?php echo $industries_item['link']['url']?>">View More <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <div class="section-footer">
            <?php if (!empty($industries['link']['url'])): ?>
                <a href="<?php echo $industries['link']['url']; ?>" class="btn btn-outline-primary"><?php echo !empty($industries['link']['title']) ? $industries['link']['title']:'Apply now'; ?></a>
            <?php endif; ?>
        </div>
    </div>
</div>
