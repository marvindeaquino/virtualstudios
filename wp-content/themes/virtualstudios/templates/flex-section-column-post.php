<?php $section_column_post = get_sub_field('section_column_post');?>
<div class="section-column-post">
    <div class="container">
        <?php if ($post_simples = $section_column_post['post_simple']): ?>
            <div class="post-simple-holder">
                <?php foreach ($post_simples as $post_simple): ?>
                    <div class="post-simple">
                        <div class="post-img" style="background-image:url(<?php echo $post_simple['image']['url']?>)">
                            <?php if (!empty($post_simple['link']['url'])): ?>
                                <a href="<?php echo $post_simple['link']['url'];?>"><?php echo $post_simple['content']?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div>