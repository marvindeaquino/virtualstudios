<?php use Roots\Sage\Titles; ?>

<?php if (!is_front_page() ): ?>
<div class="page-header <?php if(get_the_post_thumbnail_url()){echo 'has-bg';} ?>" style="background-image: url('<?php the_post_thumbnail_url(); ?>')">
  <h1><?= Titles\title(); ?></h1>
</div>
<?php endif ?>
