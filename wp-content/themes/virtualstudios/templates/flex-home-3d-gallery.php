<?php $gallery = get_sub_field('gallery'); ?>
<div class="section-3d">
    <?php if(!empty($gallery['section_title'])): ?>
        <div class="section-title-o"><?php echo $gallery['section_title']; ?></div>
    <?php endif; ?>
    <?php $items = $gallery['item'];?>
    <div class="item-block-holder">
      <?php if ($items): ?>

        <?php foreach ($items as $item):?>
          <div class="item-block">
            <div class="item-copy">
              <h2><?php echo $item['title']; ?></h2>

              <div class="item-copy-hide">
                <?php echo $item['content']; ?>
                <?php $cloned_link_repeater = $item['cloned_link_repeater']; ?>
                <?php if ($cloned_link_repeater): ?>
                  <?php foreach ($cloned_link_repeater as $keys => $cloned_link_repeater_item):?>
                    <?php if (!empty($cloned_link_repeater_item['link']['url'])):?>
                      <?php if (($cloned_link_repeater_item['fancybox'] && $cloned_link_repeater_item['virtual_3d'])): ?>
                        <a class="btn btn-<?php echo ($keys == 0)? 'outline-light':'link'; ?>" data-fancybox data-type="iframe" href="javascript:;" data-src="<?php echo $cloned_link_repeater_item['link']['url']?>" >
                          <?php echo !empty($cloned_link_repeater_item['link']['title']) ? $cloned_link_repeater_item['link']['title']: 'Learn More'; ?><?php echo !empty($cloned_link_repeater_item['cloned_icon']) ? '<i class="fa fa-'.$cloned_link_repeater_item['cloned_icon'].'" aria-hidden="true"></i>':'';?>
                        </a>
                      <?php elseif ($cloned_link_repeater_item['fancybox'] && !$cloned_link_repeater_item['virtual_3d']):?>
                        <a class="btn btn-<?php echo ($keys == 0)? 'outline-light':'link'; ?>" data-fancybox<?php echo $cloned_link_repeater_item['fancybox'] ? '="'.$cloned_link_repeater_item['fancybox_label'].'"':''; ?> href="<?php echo $cloned_link_repeater_item['link']['url']?>">
                          <?php echo !empty($cloned_link_repeater_item['link']['title']) ? $cloned_link_repeater_item['link']['title']: 'Learn More'; ?><?php echo !empty($cloned_link_repeater_item['cloned_icon']) ? '<i class="fa fa-'.$cloned_link_repeater_item['cloned_icon'].'" aria-hidden="true"></i>':'';?>
                        </a>
                      <?php else:?>
                        <a class="btn btn-<?php echo ($keys == 0)? 'outline-light':'link'; ?>" href="<?php echo $cloned_link_repeater_item['link']['url']?>">
                          <?php echo !empty($cloned_link_repeater_item['link']['title']) ? $cloned_link_repeater_item['link']['title']: 'Learn More'; ?><?php echo !empty($cloned_link_repeater_item['cloned_icon']) ? '<i class="fa fa-'.$cloned_link_repeater_item['cloned_icon'].'" aria-hidden="true"></i>':'';?>
                        </a>
                      <?php endif;?>
                    <?php endif; ?>
                  <?php endforeach;?>
                <?php endif; ?>
              </div>
            </div>
            <?php $sub_items = $item['sub_item']?>
            <?php if ($sub_items): ?>
              <div class="owl-carousel">
                <?php foreach ($sub_items as $sub_item):?>
                  <div class="item-img" style="background-image:url('<?php echo $sub_item['image']['url']?>')"></div>
                <?php endforeach;?>
              </div>
            <?php endif; ?>
          </div>
        <?php endforeach; ?>
      <?php endif; ?>
    </div>
</div>
