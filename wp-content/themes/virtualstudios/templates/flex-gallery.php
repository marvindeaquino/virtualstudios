<div class="section-gallery ptb-40">
    <div class="container">
        <?php $gallery = get_sub_field('gallery'); ?>
        <?php $items = $gallery['item'];?>
        <?php if ($items): ?>
            <div class="row">
                <?php foreach ($items as $item):?>
                    <div class="col-md-4">
                        <?php $sub_items = $item['sub_item']?>
                        <div class="owl-carousel owl-carousel-gallery">
                            <?php if ($sub_items): ?>
                                <?php foreach ($sub_items as $sub_item):?>
                                    <img src="<?php echo $sub_item['image']['url'];?>" alt="">
                                <?php endforeach;?>
                            <?php endif;?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif;?>
        <div class="gallery-details">
            <?php echo $gallery['content']; ?>
        </div>
    </div>
</div>