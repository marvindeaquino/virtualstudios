<?php $project = get_sub_field('project'); ?>
<div class="section-project">
    <div class="container">
        <div class="row row-project">
            <div class="col-md-6"><img src="<?php echo $project['image']['url']?>" alt="<?php echo $project['image']['alt']?>" class="img-fluid"></div>
            <div class="col-md-6">
                <?php echo $project['content'] ?>
                <?php if (!empty($project['link']['url'])):?>
                    <?php if (($project['fancybox'] && $project['virtual_3d'])): ?>
                        <a class="btn btn-primary" data-fancybox data-type="iframe" href="javascript:;" data-src="<?php echo $project['link']['url']?>" >
                            <?php echo !empty($project['link']['title']) ? $project['link']['title']: 'Start Virtual 3D-Tour'; ?>
                        </a>
                    <?php elseif ($project['fancybox'] && !$project['virtual_3d']):?>
                        <a class="btn btn-primary" data-fancybox<?php echo $project['fancybox'] ? '="'.$project['fancybox_label'].'"':''; ?> href="<?php echo $project['link']['url']?>">
                            <?php echo !empty($project['link']['title']) ? $project['link']['title']: 'Start Virtual 3D-Tour'; ?>
                        </a>
                    <?php else:?>
                        <a class="btn btn-primary" href="<?php echo $project['link']['url']?>">
                            <?php echo !empty($project['link']['title']) ? $project['link']['title']: 'Start Virtual 3D-Tour'; ?>
                        </a>
                    <?php endif;?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
