<?php $tciwi = get_sub_field('three_column_items_with_image'); ?>
<?php if (!is_front_page()): ?>
    <div class="three-column-items-with-image ptb-80" <?php echo !empty($tciwi['background_color']) ? 'style="background-color:'.$tciwi['background_color'].';"' : ''; ?>>
        <div class="container">
            <h2 class="section-title"><?php echo $tciwi['title']; ?></h2>
            <?php if ($tciwi_item = $tciwi['item']):?>
                <div class="row row-tciwi-item">
                    <?php foreach ($tciwi_item as $item): ?>
                        <div class="col-md-4">
                            <img class="img-fluid" src="<?php echo $item['image']['url']?>" alt="<?php echo $item['image']['alt']?>">
                            <?php echo $item['content']; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
<?php else: ?>
    <div class="section-column-post section-column-post-home" <?php echo !empty($tciwi['background_color']) ? 'style="background-color:'.$tciwi['background_color'].';"' : ''; ?>>
        <div class="container">
            <h2 class="section-title"><?php echo $tciwi['title']; ?></h2>
            <?php if ($tciwi_item = $tciwi['item']):?>
                <div class="row">
                    <?php foreach ($tciwi_item as $item): ?>
                        <div class="col-md-4">
                            <div class="post-project text-center">
                                <div class="post-img" style="background-image:url('<?php echo $item['image']['url']?>')">
                                <span class="post-copy">
                                    <?php echo $item['content']; ?>
                                </span>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
<?php endif;?>
