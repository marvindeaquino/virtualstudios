<div class="section-404">
    <div class="container">
        <h1 class="text-center text-uppercase">Page Not Found</h1>
        <div class="alert alert-warning text-center">
            <?php _e('Sorry, but the page you were trying to view does not exist.', 'sage'); ?>
        </div>
    </div>
</div>


