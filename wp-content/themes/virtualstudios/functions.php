<?php define('assets_version','7.9')?>
<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php', // Theme customizer
  'lib/navwalker-bs.php', // WP_Bootstrap_Navwalker
  /*'lib/cpt.php',*/ // WP_Bootstrap_Navwalker
  'lib/acf.php' // WP_Bootstrap_Navwalker
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);


/**
 * Remove the content editor from ALL pages
 */
function remove_content_editor()
{
    remove_post_type_support('page', 'editor');
}
add_action('admin_head', 'remove_content_editor');


//Allow Pure Span
function override_mce_options($initArray)
{
    $opts = '*[*]';
    $initArray['valid_elements'] = $opts;
    $initArray['extended_valid_elements'] = $opts;
    return $initArray;
}
add_filter('tiny_mce_before_init', 'override_mce_options');


//Register ACF Map Keys
function my_acf_google_map_api( $api ){

    $api['key'] = 'xxx';

    return $api;

}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');
