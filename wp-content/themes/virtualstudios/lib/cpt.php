<?php

function cptui_register_my_cpts() {

  /**
   * Post Type: Industries.
   */

  $labels = array(
    "name" => __( "Industries", "sage" ),
    "singular_name" => __( "Industry", "sage" ),
  );

  $args = array(
    "label" => __( "Industries", "sage" ),
    "labels" => $labels,
    "description" => "",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "delete_with_user" => false,
    "show_in_rest" => true,
    "rest_base" => "",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive" => false,
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "exclude_from_search" => false,
    "capability_type" => "post",
    "map_meta_cap" => true,
    "hierarchical" => false,
    "rewrite" => array( "slug" => "industries", "with_front" => true ),
    "query_var" => true,
    "supports" => array( "title", "editor", "thumbnail" ),
  );

  register_post_type( "industries", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );
